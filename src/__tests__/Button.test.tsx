import React from "react";
import ReactDOM from "react-dom";
// import { render, fireEvent, waitForElement } from "@testing-library/react";
import { Button, Props } from "../Components/Button";

describe("<Button />", () => {
  // beforeEach(() => console.log('before after each "it"'));
  // // it("", () => console.log('before after each "it"'));
  it("should not regress", () => {
    return expect(2 + 4).toEqual(6);
  });

  it.skip("should fail but is skipped", () => {
    return expect(2 + 4).toEqual(7);
  });

  it('async test with "done" like a argument', done => {
    setTimeout(done, 100);
  });

  it("async test with promises", () => {
    return new Promise(resolve => setTimeout(resolve, 100));
  });

  describe("props are equal", () => {
    it("should be equal", () => {
      const testProps: Props = {
        text: "Text",
        onClickFn: () => {},
        increment: 0
      };
      const comp = (
        <Button increment={0} text="Text" onClickFn={() => {}}></Button>
      );
      console.log(comp.props);
      expect(testProps).toMatchObject(comp.props);
    });
  });

  // it("async test with async ", async () => await Axios.get("localhost:3000"));
});

// describe("<Button />", () => {
//   test("should display a blank login form, with remember me checked by default", async () => {
//     const { findAllByText } = renderLoginForm();
//     const button = await findAllByText("Button Text");

//     console.log(button);
//     expect(button);
//   });
// });

// function renderLoginForm(props: Partial<Props> = {}) {
//   const defaultProps: Props = {
//     text: "Button Text",
//     increment: 0,
//     onClickFn: increment => {
//       return increment + 1;
//     }
//   };
//   return render(<Button {...defaultProps} {...props} />);
// }
