import React from "react";

export interface Props {
  text: string | number;
  increment: number;
  onClickFn: (increment: number) => void;
}

export const Button: React.FC<Props> = props => {
  const handleClicl = () => props.onClickFn(props.increment);
  return (
    <button className="button button-blue" onClick={handleClicl}>
      {props.text}
    </button>
  );
};
