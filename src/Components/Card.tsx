import React, { Component } from "react";
import { Profile } from "../App";

interface Props {
  profile: Profile;
}
interface State {}

export default class Card extends Component<Props, State> {
  state = {};

  render() {
    return (
      <div className="max-w-sm mx-auto flex p-6 bg-white rounded-lg shadow-xl mb-4">
        <div className="bg-gray-600 rounded-full w-16 h-16">
          <img
            src={this.props.profile.avatar_url}
            // src=""
            className="rounded-full"
            alt="user profile"
          ></img>
        </div>
        <div className="pl-5 flex flex-col justify-around">
          <div className="text-gray-700 text-lg font-bold text-left">
            {this.props.profile.name}
          </div>
          <div className="text-gray-700 text-lg text-left">
            {this.props.profile.company}
          </div>
        </div>
      </div>
    );
  }
}
