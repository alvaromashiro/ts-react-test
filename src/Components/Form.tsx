import React, { Component, FormEvent } from "react";
import axios from "axios";

interface Props {
  onSubmit: any;
}
interface State {
  userName: String;
}

export default class Form extends Component<Props, State> {
  state = { userName: "" };

  handleSubmit = async (event: FormEvent) => {
    event.preventDefault();
    try {
      let response = await axios.get(
        "https://api.github.com/users/" + this.state.userName
      );
      this.setState({ userName: "" });
      this.props.onSubmit(response.data);
    } catch (error) {}
  };

  render() {
    return (
      <form
        onSubmit={this.handleSubmit}
        className="max-w-sm mx-auto flex center p-4 bg-white mb-5 h-full"
      >
        <input
          value={this.state.userName}
          type="text"
          placeholder="GitHub user"
          className="w-8/12 h-full"
          required
          onChange={event => this.setState({ userName: event.target.value })}
        />
        <button type="submit" className="w-4/12 h-full">
          Search
        </button>
      </form>
    );
  }
}
