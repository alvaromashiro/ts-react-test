import React from "react";

interface Props {
  title: String;
}

export const Title: React.FC<Props> = ({ title }) => {
  return <h1 className="text-3xl mb-8">{title}</h1>;
};
