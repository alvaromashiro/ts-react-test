import React from "react";
import Card from "./Card";
import { Profile } from "../App";

interface Props {
  profiles: Profile[];
}

export const CardList: React.FC<Props> = props => {
  return (
    <div>
      {props.profiles.map((prof: Profile) => (
        <Card profile={prof} key={prof.id} />
      ))}
    </div>
  );
};
