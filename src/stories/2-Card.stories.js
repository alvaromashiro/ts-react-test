import React from "react";
import Card from "../Components/Card";
import "../App.css";

export default {
  title: "Card"
};
export const card = () => (
  <Card
    profile={{
      avatar_url: "https://avatars2.githubusercontent.com/u/9145435?s=460&v=4",
      name: "Test",
      company: "Test"
    }}
  ></Card>
);
