import React, { useState } from "react";
// import "./App.css";
import { Button } from "./Components/Button";
import { Title } from "./Components/Title";
import { CardList } from "./Components/CardList";
import Form from "./Components/Form";

export interface Profile {
  avatar_url: string;
  name: string;
  company: string;
  id: number;
}

const App: React.FC = () => {
  const [counter, setCounter] = useState(0);
  const [profiles, setProfiles] = React.useState<Profile[]>([]);
  const handleClickMinus = (decrement: number) =>
    setCounter(counter - decrement);
  const handleClickPlus = (increment: number) =>
    setCounter(counter + increment);
  const addNewProfile = (profile: Profile) => {
    const mixProfiles = [...profiles, profile];
    setProfiles(mixProfiles);
  };
  return (
    <div className="App">
      <header className="App-header max-h-full">
        <h1>Pluralsighth React</h1>
        <div className="flex flex-row justify-between w-20">
          <Button onClickFn={handleClickMinus} text="-" increment={1}></Button>
          <button
            className="button w-2/5 transparent"
            onClick={() => setCounter(0)}
          >
            {counter}
          </button>
          <Button onClickFn={handleClickPlus} text="+" increment={1}></Button>
        </div>
      </header>
      <div className="bg-gray-100 p-20">
        <Title title="Github Cards"></Title>
        <Form onSubmit={addNewProfile} />
        <CardList profiles={profiles} />
      </div>
    </div>
  );
};

